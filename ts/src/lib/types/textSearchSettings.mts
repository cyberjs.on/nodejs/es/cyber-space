export type TextSearchSettings = {

    wildcardDelimiter?: string;

    escape?: string[];

}
