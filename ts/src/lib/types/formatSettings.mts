export type FormatSettings = {

    resolveTabulate?: boolean;

    /**
     *
     * Works only if the
     * @member resolveTabulate is false.
     */
    tabCount?: number;

}
