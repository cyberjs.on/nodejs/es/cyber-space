import { TextSearchSettings } from '../types/index.mjs';


export class TextSearch {

    static like(
        text: string,
        wantedWithWildcard: string[],
        settings: TextSearchSettings = {}
    ) {

        const resolveOptions = (settings: TextSearchSettings) => {

            if (!Reflect.has(settings, 'wildcardDelimiter')) {
                settings.wildcardDelimiter = '*';
            }

            return settings;
        };

        let pattern: RegExp;

        let stringPattern: string;

        let matchesByBeginning: RegExp;

        let matchesByEnd: RegExp;

        let matchesByInside: RegExp;

        settings = resolveOptions(settings);

        /**
         *
         * para suportar o wildcard entre duas partes procuradas,
         * deve-se usar o split pelo wildcard, o que resultaria em
         * várias peças, o que adicionará, talvez, desnecessária
         * complexidade ao algoritmo.
         */
        matchesByInside = new RegExp(
            `^\\${settings.wildcardDelimiter}.+\\${settings.wildcardDelimiter}\$`
        );

        matchesByBeginning = new RegExp(`\\${settings.wildcardDelimiter}\$`);

        matchesByEnd = new RegExp(`^\\${settings.wildcardDelimiter}`);

        return wantedWithWildcard.some(
            wanted => {

                stringPattern = this
                    .#escape4RegExpFrom(
                        wanted,
                        settings.escape
                    );

                if (matchesByInside.test(wanted)) {
                    stringPattern = stringPattern
                        .replace(new RegExp(`\\${settings.wildcardDelimiter}`, 'g'), '.+');
                } else {
                    stringPattern = stringPattern
                        .replace(new RegExp(`\\${settings.wildcardDelimiter}`, 'g'), '');

                    if (matchesByBeginning.test(wanted)) {
                        stringPattern = `^${stringPattern}`;
                    } else if (matchesByEnd.test(wanted)) {
                        stringPattern = `${stringPattern}\$`;
                    }
                }

                pattern = new RegExp(stringPattern);

                return pattern.test(text);
            }
        );
    }

    static collectIfContainsWildCard(text: string[], wildcardDelimiter = '*') {

        return text.filter(
            item => item.match(new RegExp(`\\${wildcardDelimiter}`))
        );
    }

    static #escape4RegExpFrom(text: string, chars: string[] = []) {

        let digestText = text;

        for (let char of chars) {

            digestText = digestText.replace(
                new RegExp(`[${char}]`, 'g'),
                `\\${char}`
            );
        }

        return digestText;
    }

}
