import { Word } from "./index.mjs";


export class Text {

    static buildSpaceTab(count: number): string {

        let spaces = '';

        for (let i = 0; i < count; i++) {
            spaces = Word.join(spaces, '');
        }

        return spaces;
    }

    static countBeginningSpaces(lineText: string): number {

        let count = 0;

        for (let char of lineText) {
            if (char === ' ') {
                count++;
            } else {
                break;
            }
        }

        return count;
    }

}
