export class Word {

    static join(...words: string[]): string {
        return words.join(' ');
    }

}
