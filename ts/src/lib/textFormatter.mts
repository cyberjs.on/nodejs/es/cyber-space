import { FormatSettings } from './types/index.mjs';
import { Helpers } from './helpers/namespace.mjs';


export class TextFormatter {

    #_text: string;

    #settings: FormatSettings;

    #resolveTabulate!: boolean;

    #tabCount!: number;

    constructor(text: string, settings: FormatSettings = {tabCount: 0}) {
        this.#_text = text;

        this.#settings = settings;

        if (settings) {

            if (Reflect.has(settings, 'resolveTabulate')) {
                this.#resolveTabulate = settings.resolveTabulate!;
            } else {
                this.#resolveTabulate = false;
            }

            if (Reflect.has(settings, 'tabCount')) {
                this.#tabCount = settings.tabCount!;
            } else if (!this.#resolveTabulate) {
                this.#tabCount = 0;
            }

        }

    }

    lineBreaks(): TextFormatter {
        return new TextFormatter(this.#_text.replace(/\n{3,}/g, '\n\n'), this.#settings);
    }

    paragraphTab(): TextFormatter {

        const textLines = this.#_text.replace(/^(\n+)|(\n+)$/g, '').split('\n');

        if (this.#resolveTabulate) {
            this.#resolveTabulateLoop(textLines);
        } else {
            this.#nonresolveTabulateLoop(textLines);
        }

        return new TextFormatter(textLines.join('\n'), this.#settings);
    }

    #nonresolveTabulateLoop(textLines: string[]) {

        let i = -1;

        let item: string;

        let spacesCount: number;

        while (i++ < textLines.length) {

            item = textLines[i];

            if (item) {
                spacesCount = this.#tabCount;

                item = `${Helpers.Text.buildSpaceTab(spacesCount)}${item.trim()}`;

                textLines[i] = item;
            }

        }

    }

    #resolveTabulateLoop(textLines: string[]) {

        let i = 0;

        let item: string;

        let spacesCount: number;

        let prevIndex: number;

        let previous: string;

        while (i++ < textLines.length) {

            item = textLines[i];

            if (item) {

                spacesCount = Helpers.Text.countBeginningSpaces(item);

                if (!spacesCount) {

                    prevIndex = i - 1;

                    previous = textLines[prevIndex];

                    if (!previous) {
                        prevIndex = this.#resolvePreviousLineIndex(textLines, prevIndex);
                    }

                    spacesCount = Helpers.Text.countBeginningSpaces(textLines[prevIndex]);

                }

                item = `${Helpers.Text.buildSpaceTab(spacesCount)}${item.trim()}`;

                textLines[i] = item;
            }

        }

    }

    #resolvePreviousLineIndex(textLines: string[], currentIndex: number): number {

        let previous = textLines[currentIndex];

        while (!previous && currentIndex >= 0) {
            currentIndex--;
            previous = textLines[currentIndex];
        }

        return currentIndex;
    }

    get text() {
        return this.#_text;
    }

}
