import { TextFormatter } from '../index.mjs';


const textFormatter = new TextFormatter(`
foo bar.
foo bas.

    foo bat.


foo bau.
`
, {
    // tabCount: 2
    // resolveTabulate: true
}
);


console.log('The original text');
console.log('------------');
console.log(textFormatter.text);
console.log('------------');

console.log(textFormatter.lineBreaks().paragraphTab().text);
